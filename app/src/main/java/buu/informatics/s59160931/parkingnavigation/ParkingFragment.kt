package buu.informatics.s59160931.parkingnavigation


import android.content.Context
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat.getSystemService
import androidx.databinding.DataBindingUtil
import buu.informatics.s59160931.parkingnavigation.databinding.FragmentLoginBinding
import buu.informatics.s59160931.parkingnavigation.databinding.FragmentParkingBinding

/**
 * A simple [Fragment] subclass.
 *
 */
class ParkingFragment : Fragment() {
    private lateinit var binding: FragmentParkingBinding
    private val myCar1 : MyCar = MyCar("","","")
    private val myCar2 : MyCar = MyCar("","","")
    private val myCar3 : MyCar = MyCar("","","")
    private var slot : Int = 1
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentParkingBinding>(inflater,
            R.layout.fragment_parking, container, false)
        hideButton()
        binding.apply {

            slot1Button.setOnClickListener {
                slot = 1
                showSlot(it)
            }
            slot2Button.setOnClickListener {
                slot = 2
                showSlot(it)
            }
            slot3Button.setOnClickListener {
                slot = 3
                showSlot(it)
            }
            confirmButton.setOnClickListener {
                addCar(it)
            }
            deleteButton.setOnClickListener {
                deleteCar(it)
            }
        }
        binding.myCar1 = myCar1
        binding.myCar2 = myCar2
        binding.myCar3 = myCar3
        return binding.root
    }
    private fun hideButton() {
        binding.apply {
            carnumberEdit.visibility = View.GONE
            brandEdit.visibility = View.GONE
            nameEdit.visibility = View.GONE
            confirmButton.visibility = View.GONE
            deleteButton.visibility = View.GONE
        }
    }
    private fun showSlot(view: View) {
        binding.apply {
            carnumberEdit.visibility = View.VISIBLE
            brandEdit.visibility = View.VISIBLE
            nameEdit.visibility = View.VISIBLE
            confirmButton.visibility = View.VISIBLE
            deleteButton.visibility = View.VISIBLE

            if (slot == 1){
                carnumberEdit.setText(myCar1?.carnumber)
                brandEdit.setText(myCar1?.brand)
                nameEdit.setText(myCar1?.name)
                slot1Button.setBackgroundColor(Color.RED)
            }
            else if (slot == 2){
                carnumberEdit.setText(myCar2?.carnumber)
                brandEdit.setText(myCar2?.brand)
                nameEdit.setText(myCar2?.name)
                slot2Button.setBackgroundColor(Color.RED)
            }
            else{
                carnumberEdit.setText(myCar3?.carnumber)
                brandEdit.setText(myCar3?.brand)
                nameEdit.setText(myCar3?.name)
                slot3Button.setBackgroundColor(Color.RED)
            }
        }
    }
    private fun addCar(view: View) {
        binding.apply {
            carnumberEdit.visibility = View.VISIBLE
            brandEdit.visibility = View.VISIBLE
            nameEdit.visibility = View.VISIBLE
            confirmButton.visibility = View.VISIBLE
            deleteButton.visibility = View.VISIBLE

            if (slot == 1){
                myCar1?.carnumber = carnumberEdit.text.toString()
                myCar1?.brand = brandEdit.text.toString()
                myCar1?.name = nameEdit.text.toString()
                slot1Button.setText(myCar1?.carnumber.toString())
                slot1Button.setBackgroundColor(Color.GREEN)
                invalidateAll()
            }
            else if (slot == 2){
                myCar2?.carnumber = carnumberEdit.text.toString()
                myCar2?.brand = brandEdit.text.toString()
                myCar2?.name = nameEdit.text.toString()
                slot2Button.setText(myCar2?.carnumber.toString())
                slot2Button.setBackgroundColor(Color.GREEN)
                invalidateAll()
            }
            else{
                myCar3?.carnumber = carnumberEdit.text.toString()
                myCar3?.brand = brandEdit.text.toString()
                myCar3?.name = nameEdit.text.toString()
                slot3Button.setText(myCar3?.carnumber.toString())
                slot3Button.setBackgroundColor(Color.GREEN)
                invalidateAll()
            }
            (activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view?.windowToken,0)
        }
    }
    private fun deleteCar(view: View){
        binding.apply {
            carnumberEdit.visibility = View.GONE
            brandEdit.visibility = View.GONE
            nameEdit.visibility = View.GONE
            confirmButton.visibility = View.GONE
            deleteButton.visibility = View.GONE

            if (slot == 1){
                myCar1?.carnumber = ""
                myCar1?.brand = ""
                myCar1?.name = ""
                slot1Button.setText("EMPTY")
                slot1Button.setBackgroundColor(Color.parseColor("#FFEB3B"))
                invalidateAll()
            }
            else if (slot == 2){
                myCar2?.carnumber = ""
                myCar2?.brand = ""
                myCar2?.name = ""
                slot2Button.setText("EMPTY")
                slot2Button.setBackgroundColor(Color.parseColor("#FFEB3B"))
                invalidateAll()
            }
            else{
                myCar3?.carnumber = ""
                myCar3?.brand = ""
                myCar3?.name = ""
                slot3Button.setBackgroundColor(Color.parseColor("#FFEB3B"))
                slot3Button.setText("EMPTY")
                invalidateAll()
            }
        }
    }
}
