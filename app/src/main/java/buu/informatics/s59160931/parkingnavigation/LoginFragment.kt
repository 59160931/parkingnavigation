package buu.informatics.s59160931.parkingnavigation


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.informatics.s59160931.parkingnavigation.databinding.FragmentLoginBinding
import android.app.Activity
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.ContextCompat.getSystemService
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.*
import androidx.core.content.ContextCompat.getSystemService
import androidx.navigation.ui.NavigationUI


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private const val ARG_PARAM1 = "param1"
//private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class LoginFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentLoginBinding>(inflater,
            R.layout.fragment_login, container, false)
        binding.apply {
            usernameEdit.requestFocus();
        }
        binding.loginButton.setOnClickListener { view ->
            binding.apply {
                usernameEdit.text.toString()
                passwordEdit.text.toString()
                var flag : Boolean = false
                if (usernameEdit.text.toString() == "admin" && passwordEdit.text.toString() == "1234"){
                    Toast.makeText(getActivity(), "Login Success", Toast.LENGTH_LONG).show()
                    flag = true
                }
                else if(usernameEdit.text.toString() == "" || passwordEdit.text.toString() == ""){
                    Toast.makeText(getActivity(), "Please fill all field", Toast.LENGTH_LONG).show()
                    flag = false
                    usernameEdit.setText("")
                    passwordEdit.setText("")
                    usernameEdit.requestFocus();
                }
                else{
                    Toast.makeText(getActivity(), "Username or Password incorrect", Toast.LENGTH_LONG).show()
                    flag = false
                    usernameEdit.setText("")
                    passwordEdit.setText("")
                    usernameEdit.requestFocus();
                }
                if (flag) {
                    view.findNavController().navigate(R.id.action_loginFragment_to_parkingFragment)
                }
                (activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(view?.windowToken,0)
            }
        }
        setHasOptionsMenu(true)
        return binding.root
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.options_menu, menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,
            view!!.findNavController()) || super.onOptionsItemSelected(item)
    }
}
